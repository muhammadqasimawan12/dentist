<footer class="footer">
	  <div class="container-fluid">
		    <div class="row">
			      <div class="col-md-12 footer-copyright text-center">
			        	<p class="mb-0">Copyright <?php echo e(date('Y')); ?> © Dentist </p>
			      </div>
		    </div>
	  </div>
</footer>
<?php /**PATH /var/www/html/cuba_starter_kit/resources/views/admin/simple/footer.blade.php ENDPATH**/ ?>
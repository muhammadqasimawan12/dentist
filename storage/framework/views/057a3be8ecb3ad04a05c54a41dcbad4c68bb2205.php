<?php $__env->startSection('title', 'Room Page'); ?>

<?php $__env->startSection('css'); ?>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }

    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-title'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-items'); ?>
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Room</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Room</h3>
                            
                            <a href="#" data-bs-toggle="modal" class="p-2  text-dark" data-bs-target="#exampleModalroom"  style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i> Add New Room</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                            </tr>
                            </thead>
                            <tbody>
                         <?php $__currentLoopData = $room; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <tr>
                                 <th scope="row"><?php echo e($loop->iteration); ?></th>
                                 <td><?php echo e($r->name); ?></td>
                                 <td>
                                     <?php if($r->description): ?>
                                         <?php echo e($r->description); ?>

                                         <?php else: ?>
                                         <span>---</span>
                                     <?php endif; ?>
                                 </td>
                                 <td style="text-align:right">
                                     <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                         <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                             <li>
                                                 <a href="javascript:void(0)" data-id="<?php echo e($r->id); ?>"  id="roomeditbutton" data-bs-toggle="modal" data-bs-target="#exampleModaleditroom" class=" dropdown-item" type="button"><i class="fas fa-edit"></i>
                                                     Edit
                                                 </a>
                                             </li>
                                             <li>
                                                 <a href="<?php echo e(url('/delete/'.$r->id)); ?>" type="button" class="sidebar-link sidebar-title dropdown-item deleteConfirmation"><i class="far fa-trash-alt"></i>
                                                     Delete
                                                 </a>
                                             </li>
                                         </ul>
                                     </div>
                                     
                                 </td>
                             </tr>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            <?php echo e($room->render()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/cuba_starter_kit/resources/views/admin/pages/room.blade.php ENDPATH**/ ?>
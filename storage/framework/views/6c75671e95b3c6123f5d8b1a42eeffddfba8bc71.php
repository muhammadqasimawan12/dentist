<?php $__env->startSection('title', 'Sample Page'); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/vendors/animate.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/vendors/chartist.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/vendors/date-picker.css')); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-title'); ?>
<h3>Default</h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-items'); ?>
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item active"></li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row second-chart-list third-news-update">


            <div class="col-lg-12 col-xl-12">
                <div id='calendar'></div>
            </div>



























































































































































































































































































































































































































        </div>
    </div>
    <script type="text/javascript">
        var session_layout = '<?php echo e(session()->get('layout')); ?>';
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

    <script>
        function currentDayEvents(calender, events){
            $.ajax({
                url:'index/schedule',
                type:'GET',
                datatype:'json',
                complete:function(data){
                    console.log(data)
                    calendar.FullCalendar('renderEvents')
                     $('#calendar').fullcalender('refetchEvents');
                     $('#calendar').fullCalendar( 'removeEvents');
                     calender.fullCalendar('renderEvents', events,false);
                }
                });
        }


        $( "#scheduleform" ).submit(function( event ) {
            event.preventDefault();
            $('#scheduleModal').modal('hide');
            var dval=$('#departmentID').val();
            var rval=$('#roomID').val();
            var startdateval=$('#startdateID').val();
            var enddateval=$('#enddateID').val();
            var sval=$('#starttimeID').val();
            var eval=$('#endtimeID').val();
            var multiselectemplo=$('.multiselectemployees').val();

            if(dval != '' && rval != '' && startdateval !='' && sval != '' && eval != '' && enddateval !='' && multiselectemplo != ''){
                // $.get('index/schedule',function(data){
                //     for(var i = 0; i<data.length; i++){
                //         var savestartTime = data[i].start_time;
                //         var saveendTime = data[i].end_time;
                //         console.log(savestartTime,saveendTime);

                //         if(sval >= savestartTime && sval <= saveendTime && eval >= savestartTime && eval <= saveendTime){
                //             alert('Sorry Time Slot Already Booked')
                //         }
                //         else {
                            $.ajax({
                                url:'/schedule/save',
                                type:"POST",
                                dataType:"json",
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                data: {
                                    department_id:dval,
                                    room_id:rval,
                                    start_date:startdateval,
                                    end_date:enddateval,
                                    start_time:sval,
                                    end_time:eval,
                                    employee:multiselectemplo,

                                },
                                statusCode: {
                                    500: function(xhr) {
                                        setTimeout(function(){

                                            alert('Whoops! TimeSlot Already Booked, Try Another Slot');

                                        }, 1000);

                                    }
                                },

                                complete:function(data){
                                    calender();
                                },

                            });
                    //     }
                    // }



                // })




            }

        });

        $('#scheduleeditform').submit(function(event){
            event.preventDefault();
            $('#scheduleEditModal').modal('hide');
           var action= $('#scheduleeditform').attr('action');
           console.log(action);
           var editdata=$('#scheduleeditform').serialize();
            $.ajax({
                url: action,
                data:editdata,
                dataType:"json",
                type:'PATCH',
                complete:function(data){
                    console.log(data);
                    calender();
                }
            });

        });

        $(document).on("click",".eventDeleteConfirmation",function(e) {
            var deleteID=$('#hiddendeleteid').val();
            console.log(deleteID);
            e.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#cf1b1b',
                cancelButtonColor: '#9f33f2',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:"/delete/"+deleteID,
                        type : 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        // data:{_token: csrf_token},
                        error:function(data){
                            console.log(data)
                            swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'YOUR REQUEST HAS BEEN PROCESSED',
                                showConfirmButton: false,
                                timer: 2000,
                            })
                            setTimeout(function(){
                                $('#scheduleEditModal').modal('hide');
                                calender();
                                // window.location.reload()
                            },2000)

                        },
                        // error: function(data) {
                        //     console.log(data)
                        //     Swal.fire(
                        //         'Failed!',
                        //         'error'
                        //     );
                        // },
                    });
                }
            })

        });

        function isAnOverlapEvent(info){
            console.log(info.event.extendedProps.start_time,info.event.extendedProps.end_time)
        }

        function calender(){

            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                selectable: true,
                editable: true,
                displayEventTime:true,
                slotEventOverlap:false,

                    headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                    eventTimeFormat: {
                    // like '14:30:00'
                    hour: '2-digit',
                    minute: '2-digit',
                    // second: '2-digit',
                },



                    eventClick: function (info) {

                    var id=info.event.id;
                    console.log(id)
                    var action = '/schedule/update/'+id;
                    $('#scheduleeditform').attr('action',action);
                    $('#hiddendeleteid').val(id)

                        $.get('/schedule/'+id+'/edit', function (data, mydata){
                         console.log(data);
                        $('#scheduleEditModal').modal('show');
                        $('#scheduleEditModal #departmentID').val(data.department_id);
                        $('#scheduleEditModal #roomID').val(data.room_id);
                        $('#scheduleEditModal #startdateID').val(data.start_date);
                        $('#scheduleEditModal #enddateID').val(data.end_date);
                        $('#scheduleEditModal #starttimeID').val(data.start_time);
                        $('#scheduleEditModal #endtimeID').val(data.end_time);



                        for(var i = 0; i<data.employee.length; i++) {

                            $('#scheduleEditModal .multiselectemployees').append($('<option>',
                                {
                                value: data.employee[i].id,
                                text : data.employee[i].name,
                                selected:true,
                                disabled: false,
                                }
                            ));
                             $('#scheduleEditModal .choices__list--multiple').append(
                                 '<div class="choices__item choices__item--selectable text-center" data-item="" data-id="'+data.employee[i].id+'" data-value = '+data.employee[i].id+' data-custom-properties = "'+null+'" data-deletable="" aria-selected="'+true+'"> \n' +
                                  data.employee[i].name +
                                 // '<button type="button" class="choices__button" data-button="" data-id="' +data.employee[i].id+ '"> Remove item </button>\n'+
                                 '</div>'
                             )
                        }

                    });

                    $('#scheduleEditModal').on('hidden.bs.modal', function () {
                        console.log('modal hidden')
                        $('#scheduleeditform').trigger("reset.bs.modal");
                    });
                },
                    events:'index/schedule',
                    select:function(info, args, inst)
                   {
                      var startdate = moment(info.start).format('YYYY-MM-DD');
                      var enddate = moment(info.end).subtract(1).format('YYYY-MM-DD');

                      console.log(info)
                    $('#startdateID').val(startdate);
                    $('#enddateID').val(enddate);
                     // $('#enddateID').val(info.endStr);
                     $('#scheduleModal').modal('show');

                     $('#scheduleModal').on('hidden.bs.modal', function () {
                        console.log('modal hidden')
                        $('#scheduleform').trigger("reset.bs.modal");
                    });
                    },

                   });
                  calendar.render();
        }

        document.addEventListener('DOMContentLoaded', function() {
            calender();
        });

    </script>

    

    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/locales-all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/locales-all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

    <script src="<?php echo e(asset('assets/js/chart/chartist/chartist.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/chart/chartist/chartist-plugin-tooltip.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/chart/knob/knob.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/chart/knob/knob-chart.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/chart/apex-chart/apex-chart.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/chart/apex-chart/stock-prices.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/notify/bootstrap-notify.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/dashboard/default.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/notify/index.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/datepicker/date-picker/datepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/datepicker/date-picker/datepicker.en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/datepicker/date-picker/datepicker.custom.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/typeahead/handlebars.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/typeahead/typeahead.bundle.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/typeahead/typeahead.custom.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/typeahead-search/handlebars.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/typeahead-search/typeahead-custom.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/cuba_starter_kit/resources/views/admin/pages/sample-page.blade.php ENDPATH**/ ?>
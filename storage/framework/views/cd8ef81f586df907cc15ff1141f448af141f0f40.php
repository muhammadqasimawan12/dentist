<div class="sidebar-wrapper">
	<div>
		<div class="logo-wrapper">
            <a href="<?php echo e(url('/')); ?>" class="text-dark text-decoration"> <h5 style="font-weight:700">Dentist HR</h5> </a>

			<div class="back-btn"><i class="fa fa-angle-left"></i></div>
			<div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
		</div>
		<div class="logo-icon-wrapper">
            <a href="<?php echo e(route('/')); ?>">
                <i class="fas fa-clinic-medical " style="color:#34C9EB; font-size:30px"></i>

            </a>
        </div>
		<nav class="sidebar-main">
			<div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
			<div id="sidebar-menu">
				<ul class="sidebar-links" id="simple-bar">
					<li class="back-btn">
						<a href="<?php echo e(route('/')); ?>"><img class="img-fluid" src="<?php echo e(asset('assets/images/logo/logo-icon.png')); ?>" alt=""></a>
						<div class="mobile-back text-end"><span></span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
					</li>
					<li class="sidebar-list">
                            <a class="sidebar-link sidebar-title" href="<?php echo e(url('/dashboard')); ?>">

                                <i class="fas fa-home" style="color: #7366ff; font-size:20px"></i>
                                <span class="" style="margin-left:10px">Dashboard</span>
                                
                                
                                
                            </a>
					</li>
					<li class="sidebar-list">








					</li>
                    <li class="sidebar-list">
                        <a href="<?php echo e(url('/departments')); ?>" class="sidebar-link sidebar-title">

                            <i class="far fa-building" style="color: #7366ff;font-size:20px"></i>
                            <span style="margin-left:14px"> Clinics </span>
                        </a>



                    </li>
                    <li class="sidebar-list">
                        <a href="<?php echo e(url('/departmenthead')); ?>" class="sidebar-link sidebar-title">

                            <i class="fas fa-user-friends" style="color: #7366ff; font-size:20px"></i>
                            <span style="margin-left:10px">Employee</span>
                        </a>



                    </li>
                    <li class="sidebar-list">
                        <a href="<?php echo e(url('/designation')); ?>" class="sidebar-link sidebar-title">

                            <i class="fas fa-user-tag" style="color:#7366ff; font-size:20px"></i>
                            <span style="margin-left:10px">Designation</span>
                        </a>



                    </li>

                    <li class="sidebar-list">
                        <a href="<?php echo e(url('/room')); ?>" class="sidebar-link sidebar-title">
                            
                            <i class="fas fa-laptop-house" style="color: #7366ff; font-size:20px"></i>
                            <span style="margin-left:10px">Room</span>
                        </a>
                        
                        
                        
                    </li>























				</ul>
			</div>
			<div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
		</nav>
	</div>
</div>
<?php /**PATH /var/www/html/cuba_starter_kit/resources/views/admin/simple/sidebar.blade.php ENDPATH**/ ?>
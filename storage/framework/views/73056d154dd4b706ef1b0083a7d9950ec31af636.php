<?php $__env->startSection('title', 'Department Page'); ?>

<?php $__env->startSection('css'); ?>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }
        .classonecolor1{
            background-color:#d2f7f5;
        }.classonecolor2{
             background-color:#cdcae8;
         }.classonecolor3{
              background-color:#abebac;
          }.classonecolor4{
               background-color:#e3b3d1;
           }
           .charcolor1{
               color: #43faf0;
           }
        .charcolor2{
            color:#4230d9;
           }.charcolor3{
            color: #3ce63e;

           }.charcolor4{
            color: #e645a9;
           }


    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-title'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb-items'); ?>
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Designation</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Designation</h3>

                            <a href="#" data-bs-toggle="modal" class="p-2  text-dark" data-bs-target="#exampleModaldesignation"  style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i> Add New Designation</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">No.of Employee</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $designation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $desig): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <th scope="row"><?php echo e($loop->iteration); ?></th>
                                    <td> <?php echo e($desig->title); ?></td>
                                    <td><?php echo e($desig->description); ?> </td>
                                    <td class="d-flex justify-content-center">
                                        <?php
                                            $count=0
                                        ?>
                                            <?php $__currentLoopData = $desig->department_head->where('deleteStatus',null)->take(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                                $count = $count + count(array($dh->name));
                                            ?>
                                                <div class="rounded-circle text-center vertical-align  classonecolor<?php echo e($count); ?>" style="height: 38px;width: 38px;margin-left: -10px; border: 2px solid #8ab7ff;padding-top:6px">
                                                <span class="font-weight-bold charcolor<?php echo e($count); ?>" style="font-size:13px; font-weight:700;">
                                                    <?php if($dh->name): ?>
                                                        <?php echo e($dh->name[0]); ?>

                                                    <?php else: ?>
                                                        <span>---</span>
                                                    <?php endif; ?>
                                                </span>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $totalcount=$desig->department_head->where('deleteStatus',null)->count();
                                        ?>
                                        <?php if($totalcount > 4): ?>
                                                    <span class="mt-2 " style="margin-left:5px"><?php echo e($totalcount-4); ?>+</span>
                                        <?php endif; ?>
                                    </td>
                                    <td style="text-align:right">
                                        <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                            <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                                <li>
                                                    <a href="javascript:void(0)" data-id="<?php echo e($desig->id); ?>"  id="designationeditbutton" data-bs-toggle="modal" data-bs-target="#exampleModaleditdesignation" class=" dropdown-item" type="button"><i class="fas fa-edit"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo e(url('/designationdelete/'.$desig->id)); ?>" type="button" class="sidebar-link sidebar-title dropdown-item deleteConfirmation"><i class="far fa-trash-alt"></i>
                                                        Delete
                                                    </a>

                                                </li>
                                            </ul>
                                        </div>
                                        
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            <?php echo e($designation->render()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/cuba_starter_kit/resources/views/admin/pages/designation.blade.php ENDPATH**/ ?>
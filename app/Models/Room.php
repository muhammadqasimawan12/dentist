<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function departments()
    {
        return $this->belongsToMany('App\Models\Department','department_rooms');
//        return $this->belongsToMany('App\Models\Department','department_rooms','room_id','department_id');
    }

    public function booking()
    {
        return $this->hasOne('App\Models\Booking','room_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentHead extends Model
{
    // departmenthead == employee
    use HasFactory;
    protected $guarded=[];

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation');
    }

    public function booking()
    {
        return $this->belongsToMany('App\Models\Booking','booking_employees');
    }
}

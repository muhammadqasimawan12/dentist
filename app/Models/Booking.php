<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function department()
    {

        return $this->belongsTo('App\Models\Department');
    }

    public function room()
    {

        return $this->belongsTo('App\Models\Room');
    }

    public function employee()
    {
        return $this->belongsToMany('App\Models\DepartmentHead','booking_employees','booking_id','employee_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $guarded=[];
// department_head = employee
    public function department_heads()
    {
        return $this->belongsTo('App\Models\DepartmentHead','department_head_id');
    }

//    public function employees()
//    {
//
//        return $this->hasMany('App\Models\DepartmentHead','department_id','id');
//
//    }

    public function rooms()
    {
        return $this->belongsToMany('App\Models\Room','department_rooms');
    }

    public function booking() {

        return $this->hasOne('App\Models\Booking','department_id');
    }

}

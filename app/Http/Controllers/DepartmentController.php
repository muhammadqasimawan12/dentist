<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepartmentRoomResource;
use App\Models\Department;
use App\Models\DepartmentHead;
use App\Models\DepartmentRoom;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Department $department)
    {

       $department= Department::where('deleteStatus',null)->simplePaginate(10);
        return view('admin.pages.departments',compact('department'));

    }

    public function create()
    {

    }

    public function store(Request $request, DepartmentHead $departmentHead)
    {
        Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
//             'room'=>'required|integer'
//            'department_head_id' => 'required',
//            'parent_department' => 'required',
        ])->validate();
        $department=new Department();
        $department->title = $request->title;
        $department->description = $request->description;
//        $department->department_head=$request->department_head;
        $department->parent_department = $request->parent_department;
        $department->department_head_id = $request->department_head_id;

        $department->save();
        $department->rooms()->sync($request->room);
//        dd(array($request->room));

        session()->flash('Success','Department Successfully Added!');
        return back();
    }

    public function show($id)
    {
        //
    }

    public function edit(Department $department)
    {
//      dd( Department::find(1)->rooms()->get()->toArray());
        return  response()->json(new DepartmentRoomResource($department));
    }
//



    public function update(Request $request, Department $department)
    {
       $data= Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
           'department_head_id' =>'',
           'parent_department' => '',
        ])->validate();
        $department->update($data);
        $department->rooms()->sync($request->room);
        session()->flash('Success','Department Successfully Updated!');
        return back();
    }

public function deletedepartmentStatus($department, Request $request){

    $d = Department::find($department);
    $d->deleteStatus = 1;
    $d->save();

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

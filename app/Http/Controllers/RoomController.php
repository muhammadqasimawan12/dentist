<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room = Room::where('deleteStatus',null)->simplePaginate(10);
        return view('admin.pages.room', compact('room'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
          Validator::make($request->all(),[
          'name'=>'required',
     ])->validate();
             $room= new Room();
             $room->name=$request->name;
             $room->description=$request->description;
             $room->save();
             session()->flash('Success','Room Successfully Created');
             return back();
    }
    public function roomdeleteStatus($room){

        $r = Room::find($room);
        $r->deleteStatus = 1;
        $r->save();

    }

    public function show($id)
    {
        //
    }

    public function edit(Room $room)
    {
        return response()->json($room);
    }


    public function update(Request $request, Room $room)
    {
        $data=Validator::make($request->all(),[
            'name'=>'required',
            'description'=>''
        ])->validate();
        $room->update($data);
        session()->flash('Success','Room Successfully Updated');
        return back();
    }



    public function destroy($id)
    {
        //
    }
}

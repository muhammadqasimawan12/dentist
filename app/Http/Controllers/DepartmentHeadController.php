<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\DepartmentHead;
use App\Models\DepartmentRoom;
use App\Models\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DepartmentHeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {



        $query = DepartmentHead::query();
        $query->when(request('status') == 'all', function ($q) {
            $q->get();
        })->when(request('status') == 'active',function ($q) {
            $q->where('status',1)->where('deleteStatus',null)->orWhere('status',null)->where('deleteStatus',null);
        })->when(request('status') == 'terminated',function ($q) {
            $q->where('status',2)->where('deleteStatus',null);
        })->when(request('status') == 'deceased',function ($q) {
            $q->where('status',3)->where('deleteStatus',null);
        })->when(request('status') == 'resigned',function ($q) {
            $q->where('status',4)->where('deleteStatus',null);
        })->when(request('status') == 'trash',function ($q) {
            $q->where('deleteStatus',1);
        })->when(request('status') == '',function ($q){
            $q->where('deleteStatus',null)->where('status',null)->get();
        });
//            ->where(function ($q){
//                if(request('status') == 'trash'){
//                    $q->where('deleteStatus' != null);
//                }else{
//                    $q->where('deleteStatus',null);
//                }
//        });
       $departmenthead=$query->simplePaginate(10);

       $employee=DepartmentHead::where('deleteStatus',null)->orderBy('id','DESC')->limit(4)->get();

        return view('admin.pages.departmenthead',compact('departmenthead','employee'));
    }

    public function create()
    {

    }


    public function store(Request $request)
    {
        Validator::make($request->all(),[
             'name' => 'required',
//            'department_id' => 'required',
//            'designation_id' => 'required',
        ])->validate();
       $departmenthead=new DepartmentHead();
       $departmenthead->name=$request->name;
       $departmenthead->department_id=$request->department_id;
       $departmenthead->designation_id=$request->designation_id;
       $departmenthead->save();
       session()->flash('Success','Staff Successfully Added!');
       return back();
    }

    public function deletedepartmentHeadStatus($departmenthead, Request $request)
    {
        $data = DepartmentHead::find($departmenthead);
        $data->deleteStatus = 1;
       $data->save();
    }

    public function show($id)
    {
        //
    }

    public function edit(DepartmentHead $departmenthead)
    {
        return response()->json($departmenthead);
    }


    public function update(Request $request, DepartmentHead $departmenthead)
    {
       $data = Validator::make($request->all(),[
            'name' => 'required',
            'department_id'=>'',
            'designation_id' => '',
        ])->validate();
        $departmenthead->update($data);
        session()->flash('Success','Staff Successfully Updated!');
        return back();
    }


    public function destroy($id)
    {
        //
    }

    public function changeemployeestatus($id, Request $request)
    {

        $chngstatus=DepartmentHead::find($id);
        $chngstatus->status=$request->status;
        $chngstatus->save();
        return back();

    }
}

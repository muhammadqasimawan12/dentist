<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Http\Resources\ShowEventResource;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result=Booking::get();
        $schedule=array();
        $daystoAdd=1;
        foreach ($result as $data){
            $schedule[] = [
                'id'=>$data['id'],
                'department_id' => $data->department['title'],
                'title' => $data->room['name']. ': ' .date('h:i a', strtotime($data['start_time'])). ' - '.date('h:i a',strtotime($data['end_time'])),
                'start' => date('Y-m-d H:i:s',strtotime($data['start_date'])),
                'end'=> Carbon::createFromFormat('Y-m-d H:i:s',$data['end_date'])->addDays($daystoAdd),
                'start_time' =>date('H:i', strtotime( $data['start_time'])),
                'end_time' => date('H:i',strtotime($data['end_time'])),
                'color'=>'#34c9eb',
                'allDay'=>true,
                'overlap'=> false,
                'slotEventOverlap'=>false,
            ];
        }
        return response()->json($schedule);
//        return response()->json($result);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $allbookedevents=Booking::where('start_date',$request->start_date)->where('end_date',$request->end_date)->get();

        $requestStartTime = date('h:i:s',strtotime($request->start_time));
        $requestEndTime   = date('h:i:s',strtotime($request->end_time));


        foreach($allbookedevents as $bookedevents)
        {
//            dd($requestStartTime, date('h:i:s', strtotime($bookedevents->start_time )));

            if ($requestStartTime >= date('h:i:s', strtotime($bookedevents->start_time )) && $requestStartTime <= date('h:i:s', strtotime($bookedevents->end_time )))
            {

                return response()->json(['message' => 'Invalid StartTime data'], 500);
                return back();
            }

            if ($requestEndTime >= date('h:i:s', strtotime($bookedevents->start_time)) && $requestEndTime <= date('h:i:s', strtotime($bookedevents->end_time )))
            {
                return response()->json(['message' => 'Invalid EndTime data'], 500);
                return back();
            }

        }

        $booking = new Booking();
        $booking->department_id=$request->department_id;
        $booking->room_id=$request->room_id;
        $booking->start_date=$request->start_date;
        $booking->end_date=$request->end_date;
        $booking->start_time=$request->start_time;
        $booking->end_time=$request->end_time;
        $booking->save();
        $booking->employee()->sync($request->employee);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit(Booking $booking)
    {

//        return response()->json($booking->employee()->get());
        return response()->json(new EventResource($booking));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Booking $booking)
    {
        $data= Validator::make($request->all(),[
            'department_id' => '',
            'room_id' => '',
            'start_date' =>'',
            'end_date' =>'',
            'start_time' => '',
            'end_time' => '',
        ])->validate();
        $booking->update($data);
        $booking->employee()->sync($request->employee);
//        session()->flash('Success','Schedule Successfully Updated!');
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($booking)
    {
        $booking=Booking::find($booking);
        $booking->delete();
        return back();
    }
}

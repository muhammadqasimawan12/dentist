<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designation=Designation::where('deleteStatus',null)->orderBy('id','DESC')->simplePaginate(10);
        return view('admin.pages.designation', compact('designation'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ])->validate();
        $designation = new Designation();
        $designation->title = $request->title;
        $designation->description = $request->description;
        $designation->deleteStatus = $request->deleteStatus;
        $designation->save();
        session()->flash('Success', 'Designation Successfully Added');
        return back();
    }


    public function show($id)
    {
        //
    }


    public function edit(Designation $designation)
    {
        return response()->json($designation);
    }

    public function update(Request $request, Designation $designation)
    {
        $data=Validator::make($request->all(),[
            'title' => 'required',
            'description' => ''
        ])->validate();
        $designation->update($data);
        session()->flash('Success', 'Designation Successfully updated');
        return back();
    }
    public function designationdeleteStatus($designation){
        $d = Designation::find($designation);
        $d->deleteStatus = 1;
        $d->save();
    }

    public function destroy($id)
    {
        //
    }
}

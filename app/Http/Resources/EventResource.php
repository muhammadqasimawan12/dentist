<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
              'id'=>$this->id,
              'department_id' => $this->department_id,
              'room_id' => $this->room_id,
              'start_date' => date('Y-m-d',strtotime($this->start_date)),
              'end_date' => date('Y-m-d',strtotime($this->end_date)),
              'start_time' => $this->start_time,
              'end_time' => $this->end_time,
              'employee'=>$this->employee()->get(),
        ];
//        return parent::toArray($request);
    }
}

<?php

use App\Http\Controllers\BookingController;
use App\Http\Controllers\DepartmentHeadController;
use App\Http\Controllers\DesignationController;
use App\Http\Controllers\RoomController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Session;

Route::get('/', function () {
    return redirect('/dashboard');
})->name('/');

//Language Change
Route::get('lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'de', 'es','fr','pt', 'cn', 'ae'])) {
        abort(400);
    }
    Session()->put('locale', $locale);
    Session::get('locale');
    return redirect()->back();
})->name('lang');

Route::prefix('dashboard')->group(function () {
//    Route::view('index', 'dashboard.index')->name('index');
//    Route::view('dashboard-02', 'dashboard.dashboard-02')->name('dashboard-02');
});

Route::prefix('page-layouts')->group(function () {
//    Route::view('box-layout', 'page-layout.box-layout')->name('box-layout');
//    Route::view('layout-rtl', 'page-layout.layout-rtl')->name('layout-rtl');
//    Route::view('layout-dark', 'page-layout.layout-dark')->name('layout-dark');
//    Route::view('hide-on-scroll', 'page-layout.hide-on-scroll')->name('hide-on-scroll');
//    Route::view('footer-light', 'page-layout.footer-light')->name('footer-light');
//    Route::view('footer-dark', 'page-layout.footer-dark')->name('footer-dark');
//    Route::view('footer-fixed', 'page-layout.footer-fixed')->name('footer-fixed');
});

//Route::view('sample-page', 'pages.sample-page')->name('sample-page');
//Route::view('landing-page', 'pages.landing-page')->name('landing-page');

Route::prefix('others')->group(function () {
    Route::view('400', 'errors.400')->name('error-400');
    Route::view('401', 'errors.401')->name('error-401');
    Route::view('403', 'errors.403')->name('error-403');
    Route::view('404', 'errors.404')->name('error-404');
    Route::view('500', 'errors.500')->name('error-500');
    Route::view('503', 'errors.503')->name('error-503');
});

Route::prefix('layouts')->group(function () {
//    Route::view('compact-sidebar', 'admin_unique_layouts.compact-sidebar'); //default //Dubai
//    Route::view('box-layout', 'admin_unique_layouts.box-layout');    //default //New York //
//    Route::view('dark-sidebar', 'admin_unique_layouts.dark-sidebar');
////
//    Route::view('default-body', 'admin_unique_layouts.default-body');
//    Route::view('compact-wrap', 'admin_unique_layouts.compact-wrap');
//    Route::view('enterprice-type', 'admin_unique_layouts.enterprice-type');
//
//    Route::view('compact-small', 'admin_unique_layouts.compact-small');
//    Route::view('advance-type', 'admin_unique_layouts.advance-type');
//    Route::view('material-layout', 'admin_unique_layouts.material-layout');
//
//    Route::view('color-sidebar', 'admin_unique_layouts.color-sidebar');
//    Route::view('material-icon', 'admin_unique_layouts.material-icon');
//    Route::view('modern-layout', 'admin_unique_layouts.modern-layout');
});

Route::get('/clear-cache', function() {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return "Cache is cleared";
})->name('clear.cache');


// Departments Routes Start
Route::get('/departments', [\App\Http\Controllers\DepartmentController::class,'index']);
Route::post('/departments/store', [\App\Http\Controllers\DepartmentController::class,'store']);
Route::get('/departments/{department}/edit', [\App\Http\Controllers\DepartmentController::class,'edit']);

Route::get('/rooms/{department}/edit', [\App\Http\Controllers\DepartmentController::class,'room']);

Route::patch('/departments/{department}', [\App\Http\Controllers\DepartmentController::class,'update']);
Route::patch('/departmentStatus/{department}',[\App\Http\Controllers\DepartmentController::class,'deletedepartmentStatus']);

// Departments Routes End

//Department Head route start
Route::post('/departmenthead/store',[\App\Http\Controllers\DepartmentHeadController::class,'store']);
Route::get('/departmenthead',[\App\Http\Controllers\DepartmentHeadController::class,'index']);
Route::get('/departmentshead/{departmenthead}/edit', [\App\Http\Controllers\DepartmentHeadController::class,'edit']);
Route::patch('/departmentshead/{departmenthead}', [\App\Http\Controllers\DepartmentHeadController::class,'update']);
Route::patch('/departmentheaddelete/{departmenthead}',[\App\Http\Controllers\DepartmentHeadController::class,'deletedepartmentHeadStatus']);
//department routes end

Auth::routes(['verify' => true]);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//dashboard routes



Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'dashboard'])->name('dashboard');



// designation routes start
Route::get('/designation', [App\Http\Controllers\DesignationController::class, 'index'])->name('designation');
Route::post('/designation',[App\Http\Controllers\DesignationController::class, 'store'])->name('designation.store');
Route::get('/designation/{designation}/edit',[App\Http\Controllers\DesignationController::class, 'edit'])->name('designation.edit');
Route::patch('/designation/{designation}',[App\Http\Controllers\DesignationController::class, 'update'])->name('designation.update');
Route::patch('/designationdelete/{designation}',[App\Http\Controllers\DesignationController::class, 'designationdeleteStatus']);
// designation routes end



// room route start
Route::get('/room',[App\Http\Controllers\RoomController::class, 'index']);
Route::post('/room',[App\Http\Controllers\RoomController::class, 'store'])->name('room.store');
Route::get('/room/{room}/edit',[App\Http\Controllers\RoomController::class, 'edit'])->name('room.edit');
Route::patch('/room/{room}',[App\Http\Controllers\RoomController::class, 'update'])->name('room.update');
Route::patch('/delete/{room}',[App\Http\Controllers\RoomController::class,'roomdeleteStatus']);
// room route end



// change status routes
Route::patch('/changeStatus/{id}',[DepartmentHeadController::class,'changeemployeestatus']);
// change status routes end



// calender event routes start
Route::post('/schedule/save',[App\Http\Controllers\BookingController::class,'store']);
Route::get('/index/schedule',[App\Http\Controllers\BookingController::class,'index']);
Route::get('/schedule/{booking}/edit',[BookingController::class,'edit']);
Route::PATCH('/schedule/update/{booking}',[BookingController::class,'update']);
Route::DELETE('/delete/{booking}',[App\Http\Controllers\BookingController::class,'destroy']);
// calender event routes end

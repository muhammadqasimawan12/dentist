@extends('admin.layouts.app')
@section('title', 'Department Page')

@section('css')
    .container {
    padding: 2rem 0rem;
    }

    h4 {
    margin: 2rem 0rem 1rem;
    }


@endsection

@section('style')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

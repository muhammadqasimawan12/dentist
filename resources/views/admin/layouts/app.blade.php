<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
{{--    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">--}}
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" integrity="sha512-vSyPWqWsSHFHLnMSwxfmicOgfp0JuENoLwzbR+Hf5diwdYTJraf/m+EKrMb4ulTYmb/Ra75YmckeTQ4sHzg2hg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <title>Dentist-HRM</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/e3c44fa1ae.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/5.0.0/umd/popper.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
    <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    @include('admin.simple.css')
    @yield('style')
    <style>
        textarea {
            background-color: white !important;
        }
        .page-wrapper .page-body-wrapper{
            background-color:white;
        }
        .mt-100 {
            margin-top: 100px
        }

        body {
            background: #00B4DB;
            background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
            background: linear-gradient(to right, #0083B0, #00B4DB);
            color: #514B64;
            min-height: 100vh
        }
        #customizer-links{
            display: none !important;
        }
        .choices__list--multiple .choices__item[data-deletable]{
            padding-right: 10px !important;
        }


    </style>
</head>
<body @if(Route::current()->getName() == 'index') onload="startTime()" @endif>
@if(Route::current()->getName() == 'index')
    <div class="loader-wrapper">
        <div class="loader-index"><span></span></div>
        <svg>
            <defs></defs>
            <filter id="goo">
                <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
                <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"> </fecolormatrix>
            </filter>
        </svg>
    </div>
@endif
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
@include('admin.simple.header')
<!-- Page Header Ends  -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
    @include('admin.simple.sidebar')
    <!-- Page Sidebar Ends-->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-title">
                    <div class="row">
                        <div class="col-6">
                            @yield('breadcrumb-title')
                        </div>
                        <div class="col-6">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('/') }}"> <i data-feather="home"></i></a></li>
                                @yield('breadcrumb-items')
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-info" id="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    @if (session()->has('Success'))
                    <div class="alert alert-success" id="alert">
                        <span>{{session()->get('Success')}}</span>
                    </div>
                @endif

                    <div class=" alert alert-danger   w-50 ml-auto mr-auto d-none  invaliddate-alert" style="border-radius: 10px">
                        <span class="Text-White font-weight-bold">Whooops! TimeSlot Already Booked, Try Another Slot</span>
                    </div>


            </div>
            <!-- Container-fluid starts-->
        @yield('content')


            {{-- department  start--}}
            <form action="{{url('/departments/store')}}" method="post">
            <div class="modal fade " id="exampleModal" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Clinic</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{old('title')}}" name="title" placeholder="Enter Title">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control"  id="" placeholder="Description">{{old('description')}}</textarea>
                                </div>
                            <div class="row mt-2">
                                <div class="from-group col-6">
                                    <label for="department_head">Clinic Head</label>
                                    <select type="select" name="department_head_id" id="" class="form-select">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\DepartmentHead::where('deleteStatus', null)->where('status', null)->orWhere('status',1)->get() as $name)
                                            <option value="{{$name->id}}" @if (old('department_head_id') == $name->id) selected @endif>{{$name->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="from-group col-6">
                                    <label for="parent_department">Parent Clinic</label>
                                    <select type="select" name="parent_department" id="" class="form-select">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $d)
                                            <option value="{{$d->title}}" @if (old('parent_department') == $d->title) selected @endif>{{$d->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row align-items-center mt-1">
                                <div class="form-group col-10">
                                    <label for="room">Rooms</label>
                                    <select id="choices-multiple-remove-button" name="room[]"  class="" placeholder="Select" multiple>
                                        @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)
                                            <option value="{{$room->id}}" @if (old('room') == $room->id) selected @endif>{{$room->name}}</option>
                                        @endforeach
                                    </select>
{{--                                    <select class="form-select" name="room[]" multiple aria-label="multiple select example" id="multchange">--}}
{{--                                        @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)--}}
{{--                                            <option value="{{$room->id}}">{{$room->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
                                </div>
                                <div class="form-group col-2 mt-4">
                                    <a href="" data-bs-toggle="modal"  data-bs-target="#exampleModalroom" class="text-decoration-none btn btn-sm text-white" style="background:grey">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                            <button class="btn text-white"  style=" background:#00bcd4;">Create Clinic</button>
                        </div>

                    </div>
                </div>
            </div>
            @csrf
            </form>
            {{-- department  end--}}

            {{-- department edit start--}}
            <form action="" method="post" id="editdepartment">
                @method('patch')
            <div class="modal fade " id="exampleeditModal" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h5 class="modal-title">Clinic</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" id="title" class="form-control" name="title" placeholder="Enter Title">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" class="form-control" placeholder="Description"></textarea>
                                </div>
                            <div class="row mt-2">
                                <div class="from-group col-6">
                                    <label for="department_head">Clinic Head</label>
                                    <select type="select" name="department_head_id" id="department_head_id" class="form-select" placeholder="Select">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\DepartmentHead::where('deleteStatus', null)->where('status', null)->orWhere('status',1)->get() as $name)
                                            <option value="{{$name->id}}">{{$name->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="from-group col-6">
                                    <label for="parent_department">Parent Clinic</label>
                                    <select type="select" name="parent_department" id="parent_department" class="form-select">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $d)
                                            <option value="{{$d->title}}">{{$d->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row align-items-center justify-content-between mt-1">
                                <div class="form-group col-10">
                                    <label for="room">Rooms</label>
                                            <select id="choices-multiple-remove-button" name="room[]" class="form-select form-control room" placeholder="Select" multiple>
                                                @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)
                                                            <option value="{{$room->id}}">{{$room->name}}</option>
                                                @endforeach
                                            </select>

{{--                                    <select class="form-select" name="room[]"multiple aria-label="multiple select example" id="room">--}}
{{--                                        <option value="">Select</option>--}}
{{--                                        @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)--}}
{{--                                                    <option value="{{$room->id}}">{{$room->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
                                </div>
                                <div class="form-group col-2 mt-4">
                                    <a href="" data-bs-toggle="modal" data-bs-target="#exampleModalroom" class="text-decoration-none btn btn-sm text-white" style="background:grey">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                            <button class="btn text-white "  style=" background:#00bcd4;">Edit Clinic</button>
                        </div>

                    </div>
                </div>
            </div>
            @csrf
            </form>
            {{-- department edit end--}}

            {{-- department head start--}}
            <form action="{{url('/departmenthead/store')}}" method="post">
            <div class="modal fade " id="exampleModalHead" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Employee</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Enter Employee">
                                </div>
                            <div class="row mt-2">
                                <div class="col-6 form-group">
                                    <label for="department_id">Clinic</label>
                                    <select name="department_id" id="department_id" class="form-select form-control">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $dep)
                                            <option value="{{$dep->id}}"  @if (old('department_id') == $dep->id) selected @endif>{{$dep->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 form-group">
                                    <label for="designation_id">Designation</label>
                                    <select name="designation_id" id="designation_id" class="form-select form-control">
                                        <option value="">Select</option>
                                        @foreach (\App\Models\Designation::where('deleteStatus',null)->get() as $desig)
                                            <option value="{{$desig->id}}" @if (old('designation_id') == $desig->id) selected @endif>{{$desig->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                            <button class="btn text-white "  style=" background:#00bcd4;">Create Employee</button>
                        </div>

                    </div>
                </div>
            </div>
            @csrf
            </form>
            {{--department head end--}}

            {{-- department head edit start  modal --}}
            <form action="" method="post" id="editheaddepartment">
                @method('patch')
            <div class="modal fade " id="exampledepatmentheadModal" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Employee</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="dname" placeholder="Enter Employee">
                                </div>
                            <div class="row mt-2">
                                <div class="col-6 form-group">
                                    <label for="department_id">Clinic</label>
                                    <select name="department_id" id="department_id" class="form-select form-control">
                                        <option value="0">Select</option>
                                        @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $dep)
                                            <option value="{{$dep->id}}">{{$dep->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 form-group">
                                    <label for="designation_id">Designation</label>
                                    <select name="designation_id" id="designation_id" class="form-select form-control">
                                        <option value="0">Select</option>
                                        @foreach (\App\Models\Designation::where('deleteStatus',null)->get() as $desig)
                                            <option value="{{$desig->id}}" >{{$desig->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                            <button class="btn text-white"  style=" background:#00bcd4;">Edit Employee</button>
                        </div>

                    </div>
                </div>
            </div>
            @csrf
            </form>
            {{-- department head edit end  modal--}}

            {{-- Designation modal start--}}
            <form action="{{url('/designation')}}" method="post">
                <div class="modal fade " id="exampleModaldesignation" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Designation</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{old('title')}}" name="title" placeholder="Enter Title">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" id="" placeholder="Description">{{old('description')}}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                                <button class="btn text-white"  style=" background:#00bcd4;">Create Designation</button>
                            </div>

                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{-- Designation modal End --}}

            {{-- Designation editmodal start--}}
            <form action="" method="post" id="editdesignation">
                @method('patch')
                <div class="modal fade " id="exampleModaleditdesignation" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Designation</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" id="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                                <button class="btn text-white "  style=" background:#00bcd4;">Edit Designation</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{-- Designation editmodal End --}}

            {{-- room modal start--}}
            <form action="{{url('/room')}}" method="post">
                <div class="modal fade modal-fullscreen-sm-down" id="exampleModalroom" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Rooms</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="Enter Name">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" id="" placeholder="Description">{{old('description')}}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                                <button class="btn text-white"  style=" background:#00bcd4;">Add Room</button>
                            </div>

                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{-- room modal end--}}

            {{-- room editmodal start--}}
            <form action="" method="post" id="editroom">
                @method('patch')
                <div class="modal fade " id="exampleModaleditroom" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Rooms</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                                </div>
                                <div class="from-group mt-2">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" id="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                                <button class="btn text-white"  style=" background:#00bcd4;">Edit Room</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{-- room editmodal end--}}

            {{-- change status modal start--}}
            <form action="" method="post" id="changestatus">
                @method('patch')
                <div class="modal fade " id="changestatusmmodal" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Status</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control form-select">
                                        <option value="" >Select</option>
                                        <option value="1">Active</option>
                                        <option value="2">Terminated</option>
                                        <option value="3">Deceased</option>
                                        <option value="4">Resigned</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal"  style=" background:grey;">Cancel</button>
                                <button class="btn text-white"  style=" background:#00bcd4;">Add Status</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{--change status modal end--}}

            {{--   Event calender modal  start       --}}
            <form action="{{url('/schedule/save')}}" method="post" id="scheduleform">

                <div class="modal fade" id="scheduleModal" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Schedule</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="department_id">Clinic</label>
                                        <select name="department_id" id="departmentID" class="form-control form-select" placeholder="Enter Department" required>
                                            <option value="">Select</option>
                                            @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $dept)
                                                <option value="{{$dept->id}}" @if (old('department_id') == $dept->id) selected @endif>{{$dept->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="room_id">Room</label>
                                        <select name="room_id" id="roomID" class="form-control form-select" placeholder="Enter Room" required>
                                            <option value="">Select</option>
                                            @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)
                                                <option value="{{$room->id}}"  @if (old('room_id') == $room->id) selected @endif>{{$room->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="form-group col-6">
                                        <label for="date">Start Date</label>
                                        <input type="date" class="form-control" name="start_date" id="startdateID" placeholder="Enter Date" required>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="date">End Date</label>
                                        <input type="date" class="form-control" name="end_date" id="enddateID" placeholder="Enter Date"  required>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="form-group col-6">
                                        <label for="start_time">Start Time</label>
                                        <input type="time" class="form-control" id="starttimeID" value="{{ old ('start_time')}}" placeholder="Start Time" name="start_time" required>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="end_time">End Time</label>
                                        <input type="time" class="form-control" id="endtimeID" value="{{   old ('end_time')}}" placeholder="End Time" name="end_time" required>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="employee">Staff</label>
                                    <select id="choices-multiple-remove-button" name="employee[]"   class="multiselectemployees" placeholder="Select" multiple required>
                                        @foreach (\App\Models\DepartmentHead::where('deleteStatus',null)->where('status',null)->orwhere('status',1)->get() as $employee)
                                            <option value="{{$employee->id}}" @if (old('employee') == $employee->id) selected @endif>{{$employee->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn text-white" data-bs-dismiss="modal" style=" background:grey;">Cancel</button>
                                <button class="btn text-white" style=" background:#00bcd4;">Add Schedule</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
            </form>
            {{--   Event calender modal  end       --}}

            {{--   Event calender Editmodal  start       --}}
            <div class="modal fade" id="scheduleEditModal" tabindex="-1">
                    <input type="hidden" value="" id="hiddendeleteid">
                    <div class="modal-dialog modal-dialog-centered modal-md">
                        <div class="modal-content">
                            <div class="modal-header justify-content-between">
                                    <h5 class="modal-title">Schedule</h5>

                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                            </div>
                            <form action="" method="post" id="scheduleeditform">
                                @method('patch')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="department_id">Clinic</label>
                                        <select name="department_id" id="departmentID" class="form-control form-select" placeholder="Enter Department" required>
                                            <option value="">Select</option>
                                            @foreach (\App\Models\Department::where('deleteStatus',null)->get() as $dept)
                                                <option value="{{$dept->id}}">{{$dept->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="room_id">Room</label>
                                        <select name="room_id" id="roomID" class="form-control form-select" placeholder="Enter Room" required>
                                            <option value="">Select</option>
                                            @foreach (\App\Models\Room::where('deleteStatus',null)->get() as $room)
                                                <option value="{{$room->id}}">{{$room->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="form-group col-6">
                                        <label for="date">Start Date</label>
                                        <input type="date" class="form-control" name="start_date" id="startdateID" placeholder="Enter Date" required>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="date">End Date</label>
                                        <input type="date" class="form-control" name="end_date" id="enddateID" placeholder="Enter Date" required>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="form-group col-6">
                                        <label for="start_time">Start Time</label>
                                        <input type="time" class="form-control" id="starttimeID" placeholder="Start Time" name="start_time" required>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="end_time">End Time</label>
                                        <input type="time" class="form-control" id="endtimeID" placeholder="End Time" name="end_time" required>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="employee">Staff</label>
                                    <select id="choices-multiple-remove-button" name="employee[]"  class="multiselectemployees " placeholder="Select"  multiple>
                                        @foreach (\App\Models\DepartmentHead::where('deleteStatus',null)->where('status',null)->orwhere('status',1)->get() as $employee)
                                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn text-white" data-bs-dismiss="modal" style=" background:grey;">Cancel</button>

                                <button class="btn text-white" style=" background:#00bcd4;">Update</button>
                            </div>
                                @csrf
                            </form>
                            <button  id="deleteschedule" class="btn btn-danger eventDeleteConfirmation" style="border-radius: 0">Delete</button>
                        </div>
                    </div>
                </div>
            {{--   Event calender Editmodal  end  --}}
        <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        @include('admin.simple.footer')

    </div>
</div>
<!-- latest jquery-->
@include('admin.simple.script')
<!-- Plugin used-->

{{--fullcalender script start--}}

{{--fullcalender script end--}}



<script type="text/javascript">
    if ($(".page-wrapper").hasClass("horizontal-wrapper")) {
        $(".according-menu.other" ).css( "display", "none" );
        $(".sidebar-submenu" ).css( "display", "block" );
    }
</script>

{{--// sweetalert alert script start--}}
<script>
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    $(document).on("click",".deleteConfirmation",function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        console.log(url)
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#cf1b1b',
            cancelButtonColor: '#9f33f2',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: url,
                    type : 'PATCH',
                   headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data:{_token: csrf_token},
                    success:function(response){
                        console.log(response)

                        swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'YOUR REQUEST HAS BEEN PROCESSED',
                            showConfirmButton: false,
                            timer: 2000
                        })
                        setTimeout(function(){
                            window.location.reload()
                        },2000)

                    },
                    error: function(data) {
                        console.log(data)
                        Swal.fire(
                            'Failed!',
                            'error'
                        );
                    },
                });
            }
        })
    });
</script>
{{--// sweetalert alert script end--}}
<script>
    $(document).ready(function(){

        var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
            removeItemButton: true,
            // maxItemCount: -1,
            // searchResultLimit:-1,
            // renderChoiceLimit:-1,
            editItems: true,
            resetScrollPosition: true,
            addItems: true,
        });
    });
</script>

<script>
    $('#exampleeditModal').on('hidden.bs.modal', function () {
        console.log('modal hidden')
        $('#editdepartment').trigger("reset.bs.modal");
        $('#exampleeditModal .choices__list').trigger('reset.bs.modal')
    });

    $('body').on("click", '#departmenteditbutton',function (){

    var deartmentId = $(this).data('id');
    console.log(deartmentId);
    var action = '/departments/'+deartmentId;
    console.log(action)
    $('#editdepartment').attr('action', action);
    console.log('/departments/'+deartmentId+'/edit')
    $.get('/departments/'+deartmentId+'/edit', function (data, mydata){
        console.log(mydata)
        $('#exampleeditModal').modal('show');
        $('#exampleeditModal #title').val(data.title);
        $('#exampleeditModal #description').val(data.description);
        $('#exampleeditModal #department_head_id').val(data.department_head_id);
        $('#exampleeditModal #parent_department').val(data.parent_department);


        for(var i=0; i<data.rooms.length; i++)
        {
            // $('#scheduleEditModal #employeeID').val(data.employee[i].name)
            $('#exampleeditModal #choices-multiple-remove-button').append("<option value='"+data.rooms[i].id+"' selected>"+data.rooms[i].name+"</option>")

            $('#exampleeditModal .choices__list--multiple').append(
                // ' <div class="choices__item choices__item--choice choices__item--selectable is-highlighted" data-select-text="Press to select" data-choice="" data-id='+data.rooms[i].id+' data-value='+data.rooms[i].id+' data-choice-selectable="" id="choices--choices-multiple-remove-button-item-choice-1" role="option" aria-selected="true">\n' +
                // data.rooms[i].name +
                // '</div>\n'

                '<div class="choices__item choices__item--selectable" data-item="" data-id='+data.rooms[i].id+' data-value = '+data.rooms[i].id+' data-custom-properties="null" data-deletable="" aria-selected="true"> \n' +
                data.rooms[i].name +
                // '<button type="button" class="choices__button" data-button="" aria-label="Remove item: '+data.rooms[i].id+'"> Remove item </button>\n'+
                '</div>'

            )

        }
    });

    // $.get('/rooms/'+deartmentId+'/edit',function(data)
    // {
    //     console.log(data)
    //     var i;
    //     for (i=0; i<data.rooms.length; i++)
    //     {
    //         $('#exampleeditModal #choices-multiple-remove-button').append("<option value=" + data.rooms[i].id + "  selected> " + data.rooms[i].name + "</option>");
    //
    //         $('#exampleeditModal .choices__list').append(
    //             '<div class="choices__item choices__item--selectable" data-item="" data-id='+data.rooms[i].id+' data-value = '+data.rooms[i].id+' data-custom-properties="null" data-deletable="" aria-selected="true"> \n' +
    //             data.rooms[i].name +
    //             '<button type="button" class="choices__button" data-button="" aria-label="Remove item: '+data.rooms[i].id+'"> Remove item </button>\n'+
    //             '</div>')
    //
    //     }
    //     const example = new Choices(element, {
    //         callbackOnCreateTemplates: function(template) {
    //             return {
    //                 item: (classNames, data) => {
    //                     return template(`
    //       <div class="${classNames.item} ${
    //                         data.highlighted
    //                             ? classNames.highlightedState
    //                             : classNames.itemSelectable
    //                     } ${
    //                         data.placeholder ? classNames.placeholder : ''
    //                     }" data-item data-id="${data.id}" data-value="${data.name}" ${
    //                         data.active ? 'aria-selected="true"' : ''
    //                     } ${data.disabled ? 'aria-disabled="true"' : ''}>
    //         <span>&bigstar;</span> ${data.label}
    //       </div>
    //     `);
    //                 },
    //
    //             };
    //         },
    //     });
    // })

});

</script>

<script>
$('body').on("click", '#departmentheadeditbutton',function (){
    var deartmentheadId = $(this).data('id');
    console.log(deartmentheadId)
    var action = '/departmentshead/'+deartmentheadId;
    $('#editheaddepartment').attr('action', action);
    $.get('/departmentshead/'+deartmentheadId+'/edit', function (data){
        console.log(data)
        $('#exampledepatmentheadModal').modal('show');
        $('#exampledepatmentheadModal #dname').val(data.name);
        $('#exampledepatmentheadModal #department_id').val(data.department_id);
        $('#exampledepatmentheadModal #designation_id').val(data.designation_id);


    });
})
</script>

<script>
$('body').on("click", '#designationeditbutton',function (){
    var designationId = $(this).data('id');
    console.log(designationId)
    var action = '/designation/'+designationId;
    $('#editdesignation').attr('action', action);
    $.get('/designation/'+designationId+'/edit', function (data){
        console.log(data)
        $('#exampleModaleditdesignation').modal('show');
        $('#exampleModaleditdesignation #title').val(data.title);
        $('#exampleModaleditdesignation #description').val(data.description);
        $('#exampleModaleditdesignation #room').val(data.room);

    });
})
</script>

<script>
$('body').on("click", '#roomeditbutton',function (){
    var roomId = $(this).data('id');
    console.log(roomId);
    var action = '/room/'+roomId;
    console.log(action)
    $('#editroom').attr('action', action);
    $.get('/room/'+roomId+'/edit', function (data){
        console.log(data)
        $('#exampleModaleditroom').modal('show');
        $('#exampleModaleditroom #name').val(data.name);
        $('#exampleModaleditroom #description').val(data.description);

    });
})
</script>

<script>
$('body').on("click", '#statusmodal',function (){
    var statusId = $(this).data('id');
    console.log(statusId);
    var action = '/changeStatus/'+statusId;
    console.log(action)
    $('#changestatus').attr('action', action);
    // $.get('/room/'+roomId+'/edit', function (data){
    //     console.log(data)
    //     $('#exampleModaleditroom').modal('show');
    //     $('#exampleModaleditroom #name').val(data.name);
    //     $('#exampleModaleditroom #description').val(data.description);
    // });
})
</script>

<script type="text/javascript">
    setTimeout(function () {
        // Closing the alert
        $('#alert').alert('close');
    }, 3000);
</script>

<script>
    $(document).on('change','#multchange',function (){
        $v=$('#multchange').val();
        console.log($v)

    })
</script>


</body>
</html>

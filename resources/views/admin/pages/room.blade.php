@extends('admin.layouts.app')
@section('title', 'Room Page')

@section('css')



@endsection

@section('style')
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }

    </style>
@endsection

@section('breadcrumb-title')

@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Room</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Room</h3>
                            {{--                            <h5>View DepartmentHead</h5>--}}
                            <a href="#" data-bs-toggle="modal" class="p-2  text-dark" data-bs-target="#exampleModalroom"  style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i> Add New Room</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                            </tr>
                            </thead>
                            <tbody>
                         @foreach ($room as $r)
                             <tr>
                                 <th scope="row">{{$loop->iteration}}</th>
                                 <td>{{$r->name}}</td>
                                 <td>
                                     @if ($r->description)
                                         {{$r->description}}
                                         @else
                                         <span>---</span>
                                     @endif
                                 </td>
                                 <td style="text-align:right">
                                     <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                         <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                             <li>
                                                 <a href="javascript:void(0)" data-id="{{$r->id}}"  id="roomeditbutton" data-bs-toggle="modal" data-bs-target="#exampleModaleditroom" class=" dropdown-item" type="button"><i class="fas fa-edit"></i>
                                                     Edit
                                                 </a>
                                             </li>
                                             <li>
                                                 <a href="{{url('/delete/'.$r->id)}}" type="button" class="sidebar-link sidebar-title dropdown-item deleteConfirmation"><i class="far fa-trash-alt"></i>
                                                     Delete
                                                 </a>
                                             </li>
                                         </ul>
                                     </div>
                                     {{--<button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>--}}
                                 </td>
                             </tr>
                         @endforeach
                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            {{$room->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection

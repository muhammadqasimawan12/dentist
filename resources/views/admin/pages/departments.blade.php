@extends('admin.layouts.app')
@section('title', 'Department Page')

@section('css')


@endsection

@section('style')
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }

    </style>

@endsection

@section('breadcrumb-title')

@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Clinic</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Clinics</h3>
                            <a href="#" data-bs-toggle="modal" class="p-2 ml-auto text-dark" data-bs-target="#exampleModal" style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i>  Add New Clinic</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Title</th>
                                <th scope="col">Clinic Head</th>
                                <th scope="col">Parent Clinic</th>
                                <th scope="col" >Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ( $department as $d)
{{--                                {{dd($d->department_heads)}}--}}
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$d->title}} </td>
                                    <td>
                                        @if ($d->department_heads)
                                            {{$d->department_heads->name}}
                                        @else
                                            <span>---</span>
                                        @endif

                                    </td>
                                    <td>
                                        @if($d->parent_department)
                                            {{$d->parent_department}}
                                        @else
                                            <span>---</span>
                                        @endif

                                    </td>
                                    <td style="text-align:right">
                                        <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                            <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                                <li>
                                                    <a href="javascript:void(0)" data-id="{{$d->id}}"  id="departmenteditbutton" data-bs-toggle="modal" data-bs-target="#exampleeditModal" class="dropdown-item" type="button">
                                                        <i class="fas fa-edit"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{url('/departmentStatus/'.$d->id)}}" type="submit" class="deleteConfirmation dropdown-item"><i class="far fa-trash-alt"></i>
                                                        Delete
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
{{--                                        <button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>--}}

                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            {{$department->render()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection

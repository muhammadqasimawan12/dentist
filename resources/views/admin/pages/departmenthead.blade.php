@extends('admin.layouts.app')
@section('title', 'Department Page')

@section('css')



@endsection

@section('style')
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }
        .btn-default{
            border: 1px solid lightgrey;
            border-radius: 0
        }

        .classonecolor1{
            background-color:#d2f7f5;
        }.classonecolor2{
             background-color:#cdcae8;
         }.classonecolor3{
              background-color:#abebac;
          }.classonecolor4{
               background-color:#e3b3d1;
           }
        .charcolor1{
            color: #43faf0;
        }
        .charcolor2{
            color:#4230d9;
        }.charcolor3{
             color: #3ce63e;

         }.charcolor4{
              color: #e645a9;
          }

          .btn-default{
              padding-left: 12px;
              padding-right:12px;
              padding-top: 4px;
              padding-bottom:4px;
          }
        .btn-default:hover{
            background-color:lightgrey;
        }
        .btnactive{
            background-color:lightgrey;
        }


    </style>
@endsection

@section('breadcrumb-title')

@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Employee</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Employee</h3>


                        <div class="d-flex justify-content-between align-items-center">
                            <form action="" method="get">
                                <div class="btn-group mb-2">
                                    <a href="{{url('/departmenthead?status=active')}}"  class="btn btn-default">Active</a>
                                    <a href="{{url('/departmenthead?status=terminated')}}"  class="btn btn-default">Terminated</a>
                                    <a href="{{url('/departmenthead?status=deceased')}}"  class="btn btn-default">Deceased</a>
                                    <a href="{{url('/departmenthead?status=resigned')}}"  class="btn btn-default">Resigned</a>
                                    <a href="{{url('/departmenthead?status=trash')}}"  class="btn btn-default">Trash</a>
                                    <a href="{{url('/departmenthead?status=all')}}"  class="btn btn-default">All</a>
                                </div>
                            </form>
                            <div class="d-flex align-items-center">
                                <div class="d-flex align-items-center" style="margin-right: 10px">
                                    @php
                                    $count=0
                                    @endphp

                                    @foreach ($employee as $item)

                                         @php
                                         $count = $count + count(array($item->name));
                                         @endphp
                                        <div class="rounded-circle text-center vertical-align  classonecolor{{$count}}" style="height: 38px;width: 38px;margin-left: -10px; border: 2px solid #8ab7ff; padding-top:6px">
                                            <span class="font-weight-bold charcolor{{$count}}" style="font-size:13px; font-weight:700;">{{$item->name[0]}}</span>
                                        </div>
                                    @endforeach
                                </div>
                                <a href="#" data-bs-toggle="modal" class="p-2 text-dark" data-bs-target="#exampleModalHead" style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i> Add New Employee</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Clinic</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ( $departmenthead as $dh)
{{--                             {{dd($dh->department)}}--}}
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$dh->name}}</td>
                                    <td>
                                        @if ($dh->designation)
                                            {{$dh->designation->title}}
                                            @else
                                            <span>---</span>
                                        @endif

                                    </td>
                                    <td>
                                        @if ($dh->department)
                                            {{$dh->department->title}}
                                        @else
                                            <span>---</span>
                                        @endif
                                    </td>
                                    <td style="text-align:right">
                                        <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                            <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                                <li>
                                                    <a href="javascript:void(0)" data-id="{{$dh->id}}"  id="departmentheadeditbutton" data-bs-toggle="modal" data-bs-target="#exampledepatmentheadModal" class=" dropdown-item" type="button"><i class="fas fa-edit"></i>
                                                    Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{url('/departmentheaddelete/'.$dh->id)}}" type="button" class="sidebar-link sidebar-title dropdown-item deleteConfirmation"><i class="far fa-trash-alt"></i>
                                                    Delete
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)" data-id="{{$dh->id}}" id="statusmodal" type="button" class="sidebar-link sidebar-title dropdown-item" data-bs-toggle="modal" data-bs-target="#changestatusmmodal" type="button"><i class="fas fa-exchange-alt"></i>
                                                    Change Status
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
{{--                                        <button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            {{$departmenthead->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection

@extends('admin.layouts.app')
@section('title', 'Department Page')

@section('css')



@endsection

@section('style')
    <style>

        .container {
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }
        .dropdown .dropdown-toggle:after{
            content:'' !important
        }
        .classonecolor1{
            background-color:#d2f7f5;
        }.classonecolor2{
             background-color:#cdcae8;
         }.classonecolor3{
              background-color:#abebac;
          }.classonecolor4{
               background-color:#e3b3d1;
           }
           .charcolor1{
               color: #43faf0;
           }
        .charcolor2{
            color:#4230d9;
           }.charcolor3{
            color: #3ce63e;

           }.charcolor4{
            color: #e645a9;
           }


    </style>
@endsection

@section('breadcrumb-title')

@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Designation</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Designation</h3>
{{--                            <h5>View DepartmentHead</h5>--}}
                            <a href="#" data-bs-toggle="modal" class="p-2  text-dark" data-bs-target="#exampleModaldesignation"  style="border: 1px solid grey; background-color:white"><i class="fas fa-plus text-primary"></i> Add New Designation</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">No.of Employee</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($designation as $desig)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td> {{$desig->title}}</td>
                                    <td>{{$desig->description}} </td>
                                    <td class="d-flex justify-content-center">
                                        @php
                                            $count=0
                                        @endphp
                                            @foreach($desig->department_head->where('deleteStatus',null)->take(4) as $dh)
                                            @php
                                                $count = $count + count(array($dh->name));
                                            @endphp
                                                <div class="rounded-circle text-center vertical-align  classonecolor{{$count}}" style="height: 38px;width: 38px;margin-left: -10px; border: 2px solid #8ab7ff;padding-top:6px">
                                                <span class="font-weight-bold charcolor{{$count}}" style="font-size:13px; font-weight:700;">
                                                    @if ($dh->name)
                                                        {{$dh->name[0]}}
                                                    @else
                                                        <span>---</span>
                                                    @endif
                                                </span>
                                                </div>
                                            @endforeach
                                        @php
                                            $totalcount=$desig->department_head->where('deleteStatus',null)->count();
                                        @endphp
                                        @if ($totalcount > 4)
                                                    <span class="mt-2 " style="margin-left:5px">{{$totalcount-4}}+</span>
                                        @endif
                                    </td>
                                    <td style="text-align:right">
                                        <div class="dropdown">
                                            <span class="dropdown-toggle " id="dropdownMenuButton1" data-bs-toggle="dropdown" style="background:none">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                            <ul class="dropdown-menu shadow-lg" aria-labelledby="dropdownMenuButton1">
                                                <li>
                                                    <a href="javascript:void(0)" data-id="{{$desig->id}}"  id="designationeditbutton" data-bs-toggle="modal" data-bs-target="#exampleModaleditdesignation" class=" dropdown-item" type="button"><i class="fas fa-edit"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{url('/designationdelete/'.$desig->id)}}" type="button" class="sidebar-link sidebar-title dropdown-item deleteConfirmation"><i class="far fa-trash-alt"></i>
                                                        Delete
                                                    </a>

                                                </li>
                                            </ul>
                                        </div>
                                        {{--<button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="p-2 mt-2">
                            {{$designation->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')

@endsection

<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new User();
        $user->name='super admin';
        $user->email='super@gmail.com';
        $user->password=Hash::make('password');
        $user->remember_token=null;
        $user->email_verified_at=null;
        $user->save();

    }
}
